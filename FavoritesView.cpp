#include "FavoritesView.h"
#include "FavoriteSearcher.h"
#include "FavoriteList.h"
#include "FavoriteWord.h"
#include "WordReader.h"

#include <QUrl>

FavoritesView::FavoritesView(QObject *parent):
    QObject(parent),
    m_progress(0)
{
    m_wordList = new FavoriteList(this);

    m_wordReader = new WordReader();
    m_wordReader->moveToThread(&m_readerThread);
    connect(&m_readerThread, &QThread::finished, m_wordReader, &QObject::deleteLater);
    m_readerThread.start();

    m_searcher = new FavoriteSearcher();
    m_searcher->moveToThread(&m_searcherThread);
    connect(&m_searcherThread, &QThread::finished, m_searcher, &QObject::deleteLater);
    m_searcherThread.start();

    connect(m_wordReader, &WordReader::progressChanged, this, &FavoritesView::setReadingProgress);
    connect(m_wordReader, &WordReader::finished, this, [&](bool success)
    {
        setTastStatus(success ? TaskStatus::Free : TaskStatus::Error);
    });
    connect(&m_updateTimer, &QTimer::timeout, this, [&]()
    {
        m_searcher->onWordsChanged(m_wordReader->words());
    });
    connect(m_searcher, &FavoriteSearcher::favoriteListCanged, m_wordList, &FavoriteList::updateFavoriteList);
}

FavoritesView::~FavoritesView()
{
    m_readerThread.quit();
    m_searcherThread.quit();

    m_readerThread.wait();
    m_searcherThread.wait();
}

void FavoritesView::setFile(const QUrl &url)
{
    setTastStatus(TaskStatus::InProgress);
    emit taskStatusChanged();
    QMetaObject::invokeMethod(m_wordReader, "runReader", Qt::QueuedConnection,
                              Q_ARG(QString, url.toLocalFile()));
    m_updateTimer.start(m_wordList->PreferListUpdateTimout);
}

void FavoritesView::stopReading()
{
    QMetaObject::invokeMethod(m_wordReader, "stop", Qt::QueuedConnection);
    m_updateTimer.stop();
}

void FavoritesView::setReadingProgress(double progress)
{
    m_progress = progress;
    emit progressChanged();
}

void FavoritesView::setTastStatus(FavoritesView::TaskStatus status)
{
    m_taskStatus = status;
    emit taskStatusChanged();
}
