import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Frame {
    id: root

    property string word
    property int count: 0
    property double percent: 0

    width: 200
    height: 50

    RowLayout {
        anchors.fill: parent
        spacing: 10

        Text {
            width: 100
            font.pixelSize: 20
            text: word + ": " + count
        }

        ProgressBar {
            Layout.fillWidth: true
            value: percent
        }
    }
}
