#ifndef FAVORITELIST_H
#define FAVORITELIST_H

#include "FavoriteWord.h"

#include <QAbstractListModel>
#include <QList>

class FavoriteList : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        Word = Qt::UserRole + 1,
        Count,
        Percent
    };

    explicit FavoriteList(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void updateFavoriteList(const QList<FavoriteWord> &newList);

    static constexpr int PreferListUpdateTimout = 500;

private:
    void deleteTailWord(int i);
    void insertWord(int pos, const FavoriteWord &word);
    int findTargetPosition(const FavoriteWord &word);

private:
    QList<FavoriteWord> m_words;
    int m_den;
};

#endif // FAVORITELIST_H
