#include "FavoritesView.h"
#include "FavoriteWord.h"
#include "FavoriteList.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle("Material");
    QQmlApplicationEngine engine;

    qRegisterMetaType<FavoriteWord>("FavoriteWord");
    qRegisterMetaType<QList<FavoriteWord>>("QList<FavoriteWord>");
    qRegisterMetaType<QHash<QString, int>>("QHash<QString,int>");
    qmlRegisterUncreatableType<FavoritesView>("Favorites", 1, 0, "FavoritesView", "");
    qmlRegisterUncreatableType<FavoriteList>("Favorites", 1, 0, "FavoriteList", "");

    FavoritesView favoritesView;
    engine.rootContext()->setContextProperty("favoritesView", &favoritesView);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
