#include "FavoriteList.h"
#include <QDebug>

FavoriteList::FavoriteList(QObject *parent):
    QAbstractListModel(parent),
    m_den(1)
{
}

QHash<int, QByteArray> FavoriteList::roleNames() const
{
    QHash<int, QByteArray> res;
    res.insert(Word, "word");
    res.insert(Count, "count");
    res.insert(Percent, "percent");
    return res;
}

int FavoriteList::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_words.size();
}

QVariant FavoriteList::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (m_words.isEmpty())
        return QVariant();

    switch (role) {
    case Word: return m_words.at(index.row()).word;
    case Count: return m_words.at(index.row()).count;
    case Percent: return static_cast<double>(m_words.at(index.row()).count) / m_den;
    }

    return QVariant();
}

void FavoriteList::updateFavoriteList(const QList<FavoriteWord> &newList)
{
    QMap<QString, int> newWords;
    m_den = 0;
    for (int i = 0; i < newList.size(); ++i)
    {
        newWords.insert(newList.at(i).word, newList.at(i).count);
        m_den += newList.at(i).count;
    }
    m_den = qMax(1, m_den);

    //delete unused words
    //update
    for (int i = m_words.size() - 1; i >= 0; --i)
    {
        int count = newWords.value(m_words.at(i).word, -1);
        if (count < 0)
        {
            deleteTailWord(i);
        }
        else
        {
            m_words[i].count = count;
            emit dataChanged(createIndex(i, 0), createIndex(i, 0));
        }
    }

    //insert new words
    for (int i = 0; i < newList.size(); ++i)
    {
        int indx = findTargetPosition(newList.at(i));
        if (indx >= 0)
        {
            insertWord(indx, newList.at(i));
        }
    }
}

void FavoriteList::deleteTailWord(int i)
{
    beginRemoveRows(QModelIndex(), i, i);
    m_words.removeAt(i);
    endRemoveRows();
}

void FavoriteList::insertWord(int pos, const FavoriteWord &word)
{
    beginInsertRows(QModelIndex(), pos, pos);
    m_words.insert(pos, word);
    endInsertRows();
}

int FavoriteList::findTargetPosition(const FavoriteWord &word)
{
    for (int i = 0; i < m_words.size(); ++i)
    {
        int dif = word.word.compare(m_words.at(i).word);
        if (dif == 0)
            return -1;

        if (dif < 0)
            return i;
    }

    return m_words.size();
}
