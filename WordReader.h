#ifndef WORDREADER_H
#define WORDREADER_H

#include <QObject>
#include <QHash>
#include <QMutex>

class WordReader : public QObject
{
    Q_OBJECT
public:
    explicit WordReader(QObject *parent = nullptr);
    QHash<QString, int> words() const;

public slots:
    void runReader(const QString &filePath);
    void stop();

signals:
    void progressChanged(double progress);
    void finished(bool success);

private:
    void updateProgress(quint64 num, quint64 den);
    bool isWord(const QString &word) const;
    QString normalized(const QString &word) const;

private:
    static constexpr int m_charsUpdateDelay = 1000;
    mutable QMutex m_mutex;

    QHash<QString, int> m_words;
    QHash<QString, int> m_tempWords;
    int m_lastUpdate;
    bool m_stopFlag;
};

#endif // WORDREADER_H
