#ifndef FAVORITESVIEW_H
#define FAVORITESVIEW_H

#include "FavoriteList.h"

#include <QObject>
#include <QThread>
#include <QList>
#include <QTimer>

class WordReader;
class FavoriteSearcher;

class FavoritesView : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double progress READ progress NOTIFY progressChanged)
    Q_PROPERTY(TaskStatus taskStatus READ taskStatus NOTIFY taskStatusChanged)
    Q_PROPERTY(FavoriteList* wordList READ wordList CONSTANT)
public:
    enum class TaskStatus
    {
        Free,
        InProgress,
        Error
    };
    Q_ENUM(TaskStatus)

    explicit FavoritesView(QObject *parent = nullptr);
    ~FavoritesView();

    Q_INVOKABLE void setFile(const QUrl &url);
    Q_INVOKABLE void stopReading();

    double progress() const {return m_progress;}
    TaskStatus taskStatus() const {return m_taskStatus;}
    FavoriteList *wordList() const {return m_wordList;}

signals:
    void progressChanged();
    void taskStatusChanged();

private slots:
    void setReadingProgress(double progress);

private:
    void setTastStatus(TaskStatus status);

private:
    static constexpr int m_favoriteListSize = 15;

    QThread m_readerThread;
    WordReader *m_wordReader;

    QThread m_searcherThread;
    FavoriteSearcher *m_searcher;
    QTimer m_updateTimer;

    double m_progress;
    TaskStatus m_taskStatus;
    FavoriteList *m_wordList;
};

#endif // FAVORITESVIEW_H
