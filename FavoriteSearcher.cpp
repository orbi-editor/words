#include "FavoriteSearcher.h"

#include <algorithm>
#include <QMultiMap>
#include <QHash>

bool compareByCount(const FavoriteWord &w1, const FavoriteWord &w2)
{
    return w1.count > w2.count;
}

FavoriteSearcher::FavoriteSearcher(QObject *parent):
    QObject(parent)
{
}

QList<FavoriteWord> FavoriteSearcher::sortFavorites(const QHash<QString, int> &words, int favoriteListSize)
{
    QList<FavoriteWord> temp;
    for (auto i = words.constBegin(); i != words.constEnd(); ++i)
    {
        temp.push_back(FavoriteWord(i.key(), i.value()));
    }

    if (temp.size() > favoriteListSize)
    {
        std::partial_sort(temp.begin(), temp.begin() + favoriteListSize, temp.end(), compareByCount);
        temp = temp.mid(0, favoriteListSize);
    }

    return temp;
}

void FavoriteSearcher::onWordsChanged(const QHash<QString, int> &words)
{
    m_favorites = sortFavorites(words, m_favoriteListSize);
    emit favoriteListCanged(m_favorites);
}
