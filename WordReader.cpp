#include "WordReader.h"

#include <QFile>
#include <QTextStream>
#include <QMutexLocker>
#include <QCoreApplication>

WordReader::WordReader(QObject *parent):
    QObject(parent),
    m_lastUpdate(0),
    m_stopFlag(true)
{
}

QHash<QString, int> WordReader::words() const
{
    QMutexLocker locker(&m_mutex);
    return m_words;
}

void WordReader::runReader(const QString &filePath)
{
    QFile f(filePath);
    if (!f.open(QFile::ReadOnly))
    {
        emit finished(false);
        return;
    }

    m_mutex.lock();
    m_words.clear();
    m_mutex.unlock();

    m_stopFlag = false;
    m_tempWords.clear();
    m_lastUpdate = 0;
    emit progressChanged(0);

    auto fileSize = f.size();
    QTextStream stream(&f);
    QString word;
    quint64 counter = 0;

    updateProgress(0, fileSize);

    while (!stream.atEnd() && !m_stopFlag)
    {
        stream >> word;
        counter += word.length() + 1;
        if (!isWord(word))
            continue;

        word = normalized(word);
        if (m_tempWords.contains(word))
            ++m_tempWords[word];
        else
            m_tempWords.insert(word, 1);

        if (counter - m_lastUpdate > m_charsUpdateDelay)
        {
             updateProgress(counter, fileSize);
             QCoreApplication::processEvents();
        }
    }

    updateProgress(fileSize, fileSize);
    emit finished(true);
}

void WordReader::stop()
{
    m_stopFlag = true;
}

void WordReader::updateProgress(quint64 num, quint64 den)
{
    QMutexLocker locker(&m_mutex);
    for (auto i = m_tempWords.constBegin(); i != m_tempWords.constEnd(); ++i)
    {
        if (m_words.contains(i.key()))
            m_words[i.key()] += i.value();
        else
            m_words.insert(i.key(), i.value());
    }

    m_tempWords.clear();
    m_lastUpdate = num;
    double progress = qMin(static_cast<double>(num) / den, 1.0);
    emit progressChanged(progress);
}

bool WordReader::isWord(const QString &word) const
{
    if (word.isEmpty())
        return false;

    for (auto ch: word)
    {
        if (!ch.isLetter())
            return false;
    }

    return true;
}

QString WordReader::normalized(const QString &word) const
{
    return word.toLower();
}
