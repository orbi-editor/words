#ifndef FAVORITEWORD_H
#define FAVORITEWORD_H

#include <QObject>

struct FavoriteWord
{
    Q_GADGET
    Q_PROPERTY(QString word MEMBER word)
    Q_PROPERTY(int count MEMBER count)
public:
    FavoriteWord():
        count(0) {}
    FavoriteWord(const QString &word_, int count_):
        word(word_),
        count(count_) {}

    QString word;
    int count;
};

Q_DECLARE_METATYPE(FavoriteWord)

#endif // FAVORITEWORD_H
