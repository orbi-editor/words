import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Dialogs 1.3

import Favorites 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Popular words")

    ColumnLayout {
        anchors.fill: parent
        spacing: 10

        Frame {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ListView {
                id: listView

                property int moveDuration: 500

                anchors.fill: parent
                clip: true
                spacing: 5
                model: favoritesView.wordList

                delegate: WordItem {
                    width: parent.width

                    word: model.word
                    count: model.count
                    percent: model.percent
                }
                displaced: Transition {
                    NumberAnimation { property: "y"; duration: listView.moveDuration }
                }
                add: Transition {
                    NumberAnimation { property: "y"; from: 500; duration: listView.moveDuration }
                }
                remove: Transition {
                    NumberAnimation { property: "y"; to: 1000; duration: listView.moveDuration }
                }
            }
        }

        Frame {
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent

                ProgressBar {
                    Layout.fillWidth: true

                    value: favoritesView.progress
                }

                Button {
                    text: {
                        switch (favoritesView.taskStatus) {
                        case FavoritesView.Free: return qsTr("Select file")
                        case FavoritesView.Error: return qsTr("Try again")
                        case FavoritesView.InProgress: return qsTr("Wait...")
                        }
                    }
                    enabled: favoritesView.taskStatus !== FavoritesView.InProgress
                    onClicked: fileDialog.open()
                }
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Select file for reading")
        folder: shortcuts.desktop
        onAccepted: favoritesView.setFile(fileDialog.fileUrl)
    }
}
