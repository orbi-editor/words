#ifndef FAVORITESEARCHER_H
#define FAVORITESEARCHER_H

#include "FavoriteWord.h"

#include <QObject>

class FavoriteSearcher : public QObject
{
    Q_OBJECT
public:
    explicit FavoriteSearcher(QObject *parent = nullptr);

    static QList<FavoriteWord> sortFavorites(const QHash<QString, int> &words, int favoriteListSize);

public slots:
    void onWordsChanged(const QHash<QString, int> &words);

signals:
    void favoriteListCanged(const QList<FavoriteWord> &words);

private:
    static constexpr int m_favoriteListSize = 15;

    QList<FavoriteWord> m_favorites;
};

#endif // FAVORITESEARCHER_H
